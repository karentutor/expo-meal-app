import React from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import MealItem from './MealItem';

const MealList = (props) => {
  // Flatlist passes this itemData into the function
  const renderMealItem = (itemData) => {
    return (
      <MealItem
        affordability={itemData.item.affordability}
        image={itemData.item.imageUrl}
        title={itemData.item.title}
        complexity={itemData.item.complexity}
        onSelectMeal={() => {
          props.navigation.navigate('MealDetailScreen', {
            mealId: itemData.item.id,
          });
        }}
        duration={itemData.item.duration}
      />
    );
  };

  return (
    <View style={styles.list}>
      <FlatList
        data={props.listData}
        keyExtractor={(item, index) => item.id}
        renderItem={renderMealItem}
        style={{ width: '100%' }}
      />
    </View>
  );
};

export default MealList;

const styles = StyleSheet.create({
  list: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
