// htis uses a npm package react-navigation-header-buttons for styling ease

import React from 'react';
import { Platform } from 'react-native';
//here we install the package below and then import
import { Ionicons } from '@expo/vector-icons';
import { HeaderButton } from 'react-navigation-header-buttons';

import Colors from '../constants/Colors';

const CustomHeaderButton = (props) => (
  // the `props` here come from <Item ... />
  // you may access them and pass something else to `HeaderButton` if you like
  <HeaderButton
    IconComponent={Ionicons}
    color={Platform.OS === 'android' ? Colors.accentColor : Colors.accentColor}
    iconSize={23}
    {...props}
  />
);

export default CustomHeaderButton;
