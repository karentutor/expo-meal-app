//import * as React from 'react';

import React, { useState } from 'react';
import { StyleSheet, useWindowDimensions } from 'react-native';
//nav
import { NavigationContainer } from '@react-navigation/native';
import { enableScreens } from 'react-native-screens';
//fonts
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font';
//ui
import Colors from './constants/Colors';
//util
import DrawerNav from './nav/utils/DrawerNav';
// redux store
//Step 2 create store and combine reducer
import { createStore, combineReducers } from 'redux';
// Step 2.5 import reducers
import mealsReducer from './store/reducers/meals';
// Step 5 import provider
import { Provider } from 'react-redux';

// performance enhancements with react native
enableScreens();

//Step 3 create root reducer
// mape reducers to keys
// access slice of state
const rootReducer = combineReducers({
  meals: mealsReducer,
});

//Step 4 create store
const store = createStore(rootReducer);

const fetchFonts = () => {
  // must return
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
  });
};

export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);
  const dimensions = useWindowDimensions();

  if (!fontLoaded) {
    // note that app loading needs a function AND a promise returning function
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setFontLoaded(true)}
        onError={(err) => console.log(err)}
      />
    );
  }

  //Step 6 wrap provider around top most component and provide store prop
  // now can interract with provider in any component
  //Step7 refactor out dummy data used to set up app here MEALS data-dummy
  return (
    <Provider store={store}>
      <NavigationContainer
        screenOptions={{
          headerStyle: {
            backgroundColor:
              Platform.OS === 'android' ? Colors.primaryColor : 'white',
          },
          headerTintColor:
            Platform.OS === 'android' ? 'white' : Colors.primaryColor,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      >
        <DrawerNav />
      </NavigationContainer>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
