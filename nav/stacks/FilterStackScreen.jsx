//import * as React from 'react';
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// screens
import FilterScreen from '../../screens/FilterScreen';

const FilterStack = createNativeStackNavigator();

function FilterStackScreen() {
  return (
    <FilterStack.Navigator>
      <FilterStack.Screen
        name="FilterScreen"
        component={FilterScreen}
        options={{ title: 'FilterScreen' }}
      />
    </FilterStack.Navigator>
  );
}

export default FilterStackScreen;
