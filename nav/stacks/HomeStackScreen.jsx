//import * as React from 'react';
import React from 'react';
import { Button } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import CategoriesScreen from '../../screens/CategoriesScreen';
import CategoryMealScreen from '../../screens/CategoryMealScreen';
import MealDetailScreen from '../../screens/MealDetailScreen';

const HomeStack = createNativeStackNavigator();

function HomeStackScreen() {
  return (
    <HomeStack.Navigator
      screenOptions={{
        // tabBarActiveTintColor: 'tomato',
        // tabBarInactiveTintColor: 'gray',
        // headerShown: false,
        headerTitleStyle: {
          fontFamily: 'open-sans-bold',
        },
      }}
    >
      <HomeStack.Screen
        name="CategoriesScreen"
        component={CategoriesScreen}
        initialRouteName="CategoriesScreen"
        options={{
          title: 'Home',
        }}
      />
      <HomeStack.Screen
        name="CategoryMealScreen"
        component={CategoryMealScreen}
      />
      <HomeStack.Screen name="MealDetailScreen" component={MealDetailScreen} />
    </HomeStack.Navigator>
  );
}

export default HomeStackScreen;
