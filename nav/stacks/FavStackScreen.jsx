//import * as React from 'react';
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { enableScreens } from 'react-native-screens';

import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font';
import CategoriesScreen from '../../screens/CategoriesScreen';
import CategoryMealScreen from '../../screens/CategoryMealScreen';
import FavoritesScreen from '../../screens/FavouritesScreen';
import MealDetailScreen from '../../screens/MealDetailScreen';

//ui
import Colors from '../../constants/Colors';

const FavStack = createNativeStackNavigator();

function FavStackScreen() {
  return (
    <FavStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Platform.OS === 'android' ? 'white' : 'white',
        },
        headerTintColor:
          Platform.OS === 'android' ? 'white' : Colors.primaryColor,
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}
    >
      <FavStack.Screen
        name="FavoritesScreen"
        component={FavoritesScreen}
        options={{ title: 'Your Favorites' }}
      />
    </FavStack.Navigator>
  );
}

export default FavStackScreen;
