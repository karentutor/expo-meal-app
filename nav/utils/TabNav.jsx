//import * as React from 'react';
import React from 'react';
//stack screens
import FavStackScreen from '../stacks/FavStackScreen';
import HomeStackScreen from '../stacks/HomeStackScreen';
// nav
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// ui
import { Ionicons } from '@expo/vector-icons';
import Colors from '../../constants/Colors';

const Tab = createBottomTabNavigator();

const TabNavigator = () => (
  <Tab.Navigator
    screenOptions={({ route }) => ({
      headerShown: false,
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === 'HomeStack') {
          iconName = 'ios-restaurant';
          focused ? (color = Colors.accentColor) : (color = 'gray');
        } else if (route.name === 'FavStack') {
          iconName = 'ios-star';
          focused ? (color = Colors.accentColor) : (color = 'gray');
        }

        // You can return any component that you like here!
        return <Ionicons name={iconName} size={size} color={color} />;
      },

      tabBarLabelStyle: {
        fontFamily: 'open-sans-bold',
      },
    })}
  >
    <Tab.Screen name="HomeStack" component={HomeStackScreen} />
    <Tab.Screen name="FavStack" component={FavStackScreen} />
  </Tab.Navigator>
);

export default TabNavigator;
