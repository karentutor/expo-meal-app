import React from 'react';
import { useWindowDimensions } from 'react-native';

//nav
import { createDrawerNavigator } from '@react-navigation/drawer';
//tabNav
import TabNav from './TabNav';
//stack screens
import FilterStackScreen from '../stacks/FilterStackScreen';
//ui
import Colors from '../../constants/Colors';

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  const dimensions = useWindowDimensions();
  return (
    <Drawer.Navigator
      initialRouteName="Home"
      screenOptions={{
        drawerType: dimensions.width >= 768 ? 'permanent' : 'front',
        headerStyle: {
          backgroundColor:
            Platform.OS === 'android' ? Colors.primaryColor : 'white',
        },
        headerTintColor:
          Platform.OS === 'android' ? 'white' : Colors.primaryColor,
        headerTitleStyle: {
          fontFamily: 'open-sans-bold',
        },
        drawerLabelStyle: {
          fontFamily: 'open-sans-bold',
        },
      }}
    >
      {/* Screens */}
      <Drawer.Screen
        name="HomeScreen"
        component={TabNav}
        options={{ title: 'Home' }}
      />
      <Drawer.Screen
        name="Filter"
        component={FilterStackScreen}
        options={{ title: 'Filter' }}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
