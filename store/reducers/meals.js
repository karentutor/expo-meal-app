// STEP 1 CREATE REDUCER

//meals reducer state object
//making as favourite and marking as favourite
// reducer is justa  function
// recieve -- current state and an action
// executes when it gets a new action

// import
//note here is where we get our initial data
// this is where wtate starts
import { MEALS } from '../../data/dummy-data';
// don't forget to import these
import { SET_FILTERS, TOGGLE_FAVORITE } from '../actions/meals';

// here we create an initial state objecct
// at fisrt no favorite meals
const initialState = {
  meals: MEALS,
  filteredMeals: MEALS,
  favoriteMeals: [],
};

// when runs and state is not defined -- will automatically load initial state
// there will be one action dispatched when it starts to run through and thus load the state
const mealsReducer = (state = initialState, action) => {
  // here is where we handle the cases for different actions that we have
  // note here we use the constant also
  switch (action.type) {
    case TOGGLE_FAVORITE:
      //add or remove items
      // I want to find out whether the mals with the Id is part of the action
      // if that is part of favorite meals if yes remove -- if not add (toggle)
      // start by finding the index of the meals =- if -1 did not find, if > or =0 part
      const existingIndex = state.favoriteMeals.findIndex(
        (meal) => meal.id === action.mealId
      );

      /* TOGGLE STATE in an array */
      // if exists -- REMOVE IT -- TOGGLE IT
      if (existingIndex >= 0) {
        // create a copy of the array
        const updatedFavMeals = [...state.favoriteMeals];
        // remove the item found at this index
        updatedFavMeals.splice(existingIndex, 1);
        //now we store it as the updated favoriteMeals
        return { ...state, favoriteMeals: updatedFavMeals };
      } else {
        // not finding it
        // meals that I want to add -- if it is found here
        const meal = state.meals.find((meal) => meal.id === action.mealId);
        // returns a new array - -takes old array and adds a new item - -then concat and add toa rray
        return { ...state, favoriteMeals: state.favoriteMeals.concat(meal) };
      }

    case SET_FILTERS:
      const appliedFilters = action.filters;
      //filter always returns a new array
      // enact our filters one by one
      const updatedFilteredMeals = state.meals.filter((meal) => {
        if (appliedFilters.glutenFree && !meal.isGlutenFree) {
          return false;
        }

        if (appliedFilters.lactoseFree && !meal.isLactoseFree) {
          return false;
        }
        if (appliedFilters.vegetarian && !meal.isVegetarian) {
          return false;
        }
        if (appliedFilters.vegan && !meal.isVegan) {
          return false;
        }
        // if no false --return true
        return true;
      });

      // just renamed to updated FilteredMeals
      return { ...state, filteredMeals: updatedFilteredMeals };

    default:
      return state;
  }
};

export default mealsReducer;
