// actions need identifiers and payloads
export const TOGGLE_FAVORITE = 'TOGGLE_FAVORITE';
export const SET_FILTERS = 'SET_FILTERS';

// function receive data and return a JS object that describes the ation
export const toggleFavorite = (id) => {
  return {
    type: TOGGLE_FAVORITE,
    // now we add extra info
    mealId: id,
  };
};

//remember that filterSettings comes from the dispatch in the component -- settles here first
// when it lands we get the filter settings which have glutFree true, isLactose false etc
// we then hand set a key to these and forward on to our reducer to take do soemting...
export const setFilters = (filterSettings) => {
  return {
    type: SET_FILTERS,
    filters: filterSettings,
  };
};
