import React, { useCallback, useEffect, useState } from 'react';
import { Button, Platform, Switch, StyleSheet, Text, View } from 'react-native';
//ui
import CustomHeaderButton from '../components/HeaderButton';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import Colors from '../constants/Colors';
//redux
import { setFilters } from '../store/actions/meals';
//redux
import { useDispatch, useSelector } from 'react-redux';

const FilterSwitch = (props) => {
  return (
    <View style={styles.filterContainer}>
      <Text>{props.label}</Text>
      {/* trackColor takes an object*/}
      <Switch
        thumbColor={Platform.OS === 'android' ? Colors.primaryColor : ''}
        trackColor={{ true: Colors.primaryColor, false: Colors.accentColor }}
        value={props.state}
        onValueChange={props.onChange}
      />
    </View>
  );
};

const FiltersScreen = (props) => {
  const [isGlutenFree, setIsGlutenFree] = useState(false);
  const [isLactoseFree, setIsLactoseFree] = useState(false);
  const [isVegan, setIsVegan] = useState(false);
  const [isVegetarian, setIsVegetarian] = useState(false);

  const { navigation } = props;

  //redux
  const dispatch = useDispatch();

  const updateFilters = () => {
    let appliedFilters = saveFilters();
    dispatch(setFilters(appliedFilters));
  };

  // only updated if its depencies change
  // ONLY will change if its depencies change
  // useRef for a variable
  // useCallback for a function to not be re-created
  const saveFilters = () => {
    // here we gather the state
    return (appliedFilters = {
      glutenFree: isGlutenFree,
      lactoseFree: isLactoseFree,
      vegan: isVegan,
      vegetarian: isVegetarian,
    });
  };

  useEffect(() => {
    // onSelect={() => {
    //   navigation.navigate('CategoryMealScreen', {
    //     categoryId: itemData.item.id,
    //   });
    props.navigation.setOptions({
      headerRight: () => (
        <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
          <Item title="search" iconName="ios-save" onPress={updateFilters} />
        </HeaderButtons>
      ),
    });
  }, [saveFilters, navigation]);
  return (
    <View style={styles.screen}>
      <Text style={styles.title}>Available Filters / Restrictions </Text>
      <FilterSwitch
        label="Gluten-free"
        state={isGlutenFree}
        onChange={(newValue) => setIsGlutenFree(newValue)}
      />
      <FilterSwitch
        label="Lactose-free"
        state={isLactoseFree}
        onChange={(newValue) => setIsLactoseFree(newValue)}
      />
      <FilterSwitch
        label="Vegan"
        state={isVegan}
        onChange={(newValue) => setIsVegan(newValue)}
      />
      <FilterSwitch
        label="Vegetarian"
        state={isVegetarian}
        onChange={(newValue) => setIsVegetarian(newValue)}
      />
    </View>
  );
};

export default FiltersScreen;

const styles = StyleSheet.create({
  screen: {
    alignItems: 'center',
  },
  filterContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '80%',
    marginVertical: 10,
  },
  title: {
    fontFamily: 'open-sans-bold',
    fontSize: 22,
    margin: 20,
    textAlign: 'center',
  },
});
