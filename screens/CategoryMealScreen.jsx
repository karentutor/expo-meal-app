import React, { useEffect } from 'react';

//redux
import { useSelector } from 'react-redux'; // allows us to select a slice of our globally managed state and use it in the component
// data
import { CATEGORIES } from '../data/dummy-data';
import MealList from '../components/MealList';
// ui
import { StyleSheet, View, Text } from 'react-native';
import DefaultText from '../components/DefaultText';

const CategoryMealScreen = ({ route, navigation }) => {
  const { categoryId } = route.params;

  // useSelector takes a function that is executed by React Redux that gets the state as an argument
  // and it then retursna any date from that state
  // use the state identified meals found in the root reducer key to get hold of that part of the state
  // for which this reducer is responsible  - so . meals  - which is then further
  // found in the initial state --- mealsReducer filteredMeals
  const availableMeals = useSelector((state) => state.meals.filteredMeals);

  const selectedCategory = CATEGORIES.find((cat) => cat.id === categoryId);

  // if zero or greater we know that this meal has this category id in its cateogryIDs array
  const displayMeals = availableMeals.filter(
    (meal) => meal.categoryIds.indexOf(categoryId) >= 0
  );

  //if no meals to display
  if (displayMeals.length === 0) {
    return (
      <View style={styles.content}>
        <DefaultText>No meals found. Maybe check your filters?</DefaultText>
      </View>
    );
  }

  useEffect(() => {
    navigation.setOptions({ title: selectedCategory.title });
  }, []);

  return <MealList listData={displayMeals} navigation={navigation} />;
};

export default CategoryMealScreen;

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
