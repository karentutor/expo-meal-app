import React from 'react';
// screens
import MealList from '../components/MealList';
//data
// import { MEALS } from '../data/dummy-data';
// redux
import { useSelector } from 'react-redux';
// ui
import { StyleSheet, View, Text } from 'react-native';
import DefaultText from '../components/DefaultText';

const FavoritesScreen = (props) => {
  // here we see all the meals
  const favMeals = useSelector((state) => state.meals.favoriteMeals);

  // if emptry
  if (favMeals.length === 0 || !favMeals) {
    return (
      <View style={styles.content}>
        <DefaultText>No Favorite meals found. Start adding some.</DefaultText>
      </View>
    );
  }

  // const favMeals = MEALS.filter((meal) => meal.id === 'm1' || meal.id === 'm2');
  return <MealList listData={favMeals} navigation={props.navigation} />;
};

export default FavoritesScreen;

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
