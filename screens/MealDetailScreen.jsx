import React, { useEffect } from 'react';
//data
// import { MEALS } from '../data/dummy-data';
//redux
import { useDispatch, useSelector } from 'react-redux';
//ui
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { ScrollView, StyleSheet, Image, Text, View } from 'react-native';
import CustomHeaderButton from '../components/HeaderButton';
import DefaultText from '../components/DefaultText';
import { toggleFavorite } from '../store/actions/meals';
import { createIconSetFromFontello } from 'react-native-vector-icons';

const ListItem = (props) => {
  return (
    <View style={styles.listItem}>
      <DefaultText>{props.children}</DefaultText>
    </View>
  );
};
const MealDetailScreen = ({ route, navigation }) => {
  // meal ID
  const { mealId } = route.params;

  // looking for a specific meal with a specific id --  not filtered
  // const selectedMeal = MEALS.find((meal) => meal.id === mealId);

  // all available meals
  const availableMeals = useSelector((state) => state.meals.meals);
  // favourite meals
  // some returns true if the specified condition returns true for at least one item in the array
  // checks whether current meal is a favorite
  // will update when select the star as useSeletor will cause a re-rendering
  const isFavoriteMeal = useSelector((state) =>
    state.meals.favoriteMeals.some((meal) => meal.id === mealId)
  );
  const selectedMeal = availableMeals.find((meal) => meal.id === mealId);

  //redux
  const dispatch = useDispatch();

  // prevent infinite looping
  // const toggleFavoriteHandler = useCallback(() => {
  //   //forward the action -- action creator
  //   dispatch(toggleFavorite(mealId));
  // }, [dispatch, mealId]);

  const toggleFavoriteHandler = () => {
    dispatch(toggleFavorite(mealId));
  };

  useEffect(() => {
    // navigation.setParams({ toggleFav: toggleFavoriteHandler });
    navigation.setOptions({
      title: selectedMeal.title,
      headerRight: () => (
        <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
          <Item
            title="search"
            iconName={isFavoriteMeal ? 'ios-star' : 'ios-star-outline'}
            onPress={toggleFavoriteHandler}
          />
        </HeaderButtons>
      ),
    });
  }, [isFavoriteMeal, selectedMeal, toggleFavoriteHandler]);

  return (
    <ScrollView>
      <Image source={{ uri: selectedMeal.imageUrl }} style={styles.image} />
      <View style={styles.details}>
        <DefaultText>{selectedMeal.duration} m</DefaultText>
        <DefaultText>{selectedMeal.complexity.toUpperCase()}</DefaultText>
        <DefaultText>{selectedMeal.affordability.toUpperCase()}</DefaultText>
      </View>
      <Text style={styles.title}>Ingredients</Text>
      {/* output a list */}
      {selectedMeal.ingredients.map((ingredient) => (
        <ListItem key={ingredient}>{ingredient}</ListItem>
      ))}
      <Text style={styles.title}>Steps</Text>
      <Text>List of steps...</Text>
      {selectedMeal.steps.map((step) => (
        <ListItem key={step}>{step}</ListItem>
      ))}

      {/* <Button
          title="Go back to categories"
          onPress={() => props.navigation.popToTop()}
        /> */}
    </ScrollView>
  );
};

export default MealDetailScreen;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 200,
  },
  details: {
    flexDirection: 'row',
    padding: 15,
    justifyContent: 'space-around',
  },
  listItem: {
    marginVertical: 10,
    marginHorizontal: 20,
    borderColor: '#ccc',
    borderWidth: 1,
    padding: 10,
  },
  title: {
    fontFamily: 'open-sans-bold',
    fontSize: 22,
    textAlign: 'center',
  },
});
